<?php
#     [HTTP_REFERER] => http://127.0.0.1/test/change-pass.php


include "includes/functions.php";

$hld = $_SERVER['HTTP_REFERER'];
$hld = explode('?', $hld);

if (isset($_POST['submit1'])) {
  if ($hld[0] == 'http://127.0.0.1/test/change-pass.php') {
    $id = $_GET['id'];
    $new = $_POST['pass'];
    $confirm = $_POST['repass'];
    change_pass($id,$new,$confirm);
  } else {
    echo "Page not found";
  }
}
?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
  <div class="row">
    <div class="col-sm-4">

      <form method="POST" action="">

        <label>New Password</label>
        <div class="form-group pass_show">
          <input type="password" name="pass" class="form-control" placeholder="New Password">
        </div>
        <label>Confirm Password</label>
        <div class="form-group pass_show">
          <input type="password" name="repass" class="form-control" placeholder="Confirm Password">
        </div>
        <input type="submit" class="btn col-md-12 mt-2" name="submit1" value="submit">

      </form>
    </div>
  </div>
</div>