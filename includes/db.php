<?php

const HOST = 'localhost';
const USER = 'root';
const PASS = '';
const DB   = 'test';
global $connection;

$connection = mysqli_connect(HOST, USER, PASS, DB);
if ($connection) {

  return $connection;
} else {
  echo "Connection error<br>";
}
