<?php
$connection = require_once "db.php";
const URL_FOR_DELETE_FUNC = 'http://127.0.0.1/test/show-users.php';

# ------------- Login function ------------- #

function login($query, $user, $pass)
{
  $query_res = run_query($query);

  if ($query_res->num_rows) {
    while ($row = mysqli_fetch_assoc($query_res)) {
      if (($row['user']) == $user && $row['pass'] == $pass) {
        echo "Authentication correct<br>";
        return true;
      }
    }
    echo "Username or password is wrong<br>";
  } else {
    echo "Query error<br>";
  }
}
# ------------- Run Query ------------- #

# Run query function: 
function run_query($query)
{
  global $connection;
  return ($connection->query($query));
}

# ------------- Show all users ------------- #

function show_users()
{
  $query = "SELECT * FROM users";
  $query_res = run_query($query);
  $row = mysqli_fetch_all($query_res);
  return $row;
}

# ------------- Delete users ------------- #

function delete_user($id)
{
  $query = "DELETE FROM users WHERE id=" . $id;
  $query_res = run_query($query);
  header("Location:" . URL_FOR_DELETE_FUNC);
}

# ------------- Change password ------------- #

function change_pass($id, $new, $confirm)
{
  echo $new;
  echo '<br>' . $confirm;
  if ($new === $confirm) {
    $query = "UPDATE users SET pass='$new' WHERE id=" . $id;
    echo $query . '<Br>';
    $query_res = run_query($query);
    header("Location:" . URL_FOR_DELETE_FUNC);
  } else {
    echo "Both must be same";
  }
}
