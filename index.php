<?php require_once "includes/db.php"; ?>
<?php require "login.php"; ?>


<?php require_once "includes/header.php"; ?>
<body>
  <div class="container">
    <div class="col-md-6 mt-4">
      <form action="login.php" method="POST">
        <div class="form-group">
          <label for="user">Username:</label>
          <input type="text" class="form-control" name="user">
        </div>
        <div class="form-group">
          <label for="pass">Password:</label>
          <input type="password" class="form-control" name="pass">
        </div>
        <button type="submit" style="width:100%;" name="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</body>

</html>