<?php require 'includes/functions.php'; ?>
<?php $hld_users = show_users(); ?>


<?php require_once "includes/header.php"; ?>

<body>

  <table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Username</th>
        <th scope="col">Password</th>
        <th scope="col">Edit</th>
        <th scope="col">Change pw</th>
        <th scope="col">Delete</th>

      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($hld_users as $key => $value) {
          ?>
          <tr>
            <th scope="row"><?php echo $value[0]; ?></th>
            <td><?php echo $value[1]; ?></td>
            <td><?php echo $value[2]; ?></td>
            <td><a href="">Edit</a></td>
            <td><a href="change-pass.php?id=<?= $value[0]; ?>" type='button' class='btn btn-info' >Change</a></td>
            <td><a onClick="return confirm('Are you sure you want to delete?')" href="delete-users.php?del=<?= $value[0]; ?>" type='button' class='btn btn-danger' >Delete</a></td>
          </tr>
        </tbody>
      <?php } ?>
  </table>

</body>

</html>